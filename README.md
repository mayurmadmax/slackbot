# README #

* Review monitor for android apps
* Build a Slack bot that takes the bundle id of an Android app as the input and gives all the app reviews and average app rating in the last 7 days. 
* Use Node js and redis mainly in the backend. 
* Overall design(UX and Code) on the chat interaction between the bot and user you can decide. Go crazy in terms of features etc.
* Bonus point if you can visualise a good review monitor(alerts when someone rates 3 or less, etc).
* Share the codebase in a zip file and deploy the app in heroku. I should be able to install the slack bot in one of my slack channels and play with it.
* You can use npm modules when needed. Think through the authentication and API structure very clearly.
* Also explain what you would do to scale this review monitor if we have about 100k DAUs with 20k concurrent users max