require('app-module-path').addPath(__dirname);
var Botkit = require('botkit');
var processor = require('controller/reviews.js');
var controller = Botkit.slackbot({
    debug: false
});

// connect the bot to a stream of messages
controller.spawn({
   token:  process.env.BOT_API_KEY || 'xoxb-265083704576-x5NVPd9YGwIvkyVBANwoSaXf'
}).startRTM();

/* Bot to listen */
controller.hears('bundle',    ['direct_message','direct_mention','mention'],function(bot,message) {
	var index, id;
	id = (message.raw_message.text + "").toLowerCase();
	id = id.split(" ");
	index = id.indexOf('bundle');
	id.splice(index, 1);
	console.log("ID:", id);
	if(id && id[0]) {
		if(id[0] == 'flush'){
			processor.flushRedisDb(function(e, data) {
				bot.reply(message,data);	
			})
		} else if(id[0] == "check"){
			bot.reply(message,"All OK");
		} else {
		    processor.compute(id[0], function(e, reviews) {
		    	console.log("EEE:", e, reviews);
		    	if(e) {
		    		bot.reply(message,'some error! come again...');	
		    	} else {
			    	bot.reply(message,JSON.stringify(reviews.fetchData[0]));
			    	bot.reply(message,"Average: " + (reviews.fetchData[1] || '-'));
			    }
		    });
		}
	} else {
		bot.reply(message,'How can I help you ! Let\'s more precise.');	
	}
});