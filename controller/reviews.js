require('../helper/redis').init();
var async = require('async'),
    c = require('../helper/common'),
    redis = require('../helper/redis'),
    REVIEWS_PREFIX = 'gpr_',
    LAST_ID = 'last_inserted_gprid_',
    daysToGoBack = process.env.DAYS_TO_GO_BACK || 7;

var exportMe = {
    compute: function(id, parentCb) {
        var q, pageNo = [0],
            firstValue;
        async.auto({
            getLastSavedId: function(cb) {
                redis.get(LAST_ID + id, cb);
            },
            save2redis: ['getLastSavedId', function(lsid, cb) {
                q = async.queue(function(task, qCallback) {
                    // console.log("pageNo1:", pageNo);
                    c.fetchReviews(id, pageNo[0], null, function(e, results) {
                    	if(results.gpReview && results.gpReview.length == 0) {
                    		return cb('NoRecord');
                    	}
                        if (e) {
                            return cb(e);
                        } else {
                            // save it
                            if (!lsid.getLastSavedId) {
                                redis.set(LAST_ID + id, results && results.gpReview && results.gpReview[0] && results.gpReview[0].id);
                            }
                            !firstValue && (firstValue = results.gpReview[0].id);
                            exportMe.processRecords(id, lsid, results, pageNo, q, cb, firstValue, false, qCallback);
                        }
                    });
                }, 1);
                q.push(++pageNo[0]);
            }],
            fetchData: ['save2redis', function(results, cb) {
                var reviewsDatewiseData = [],
                	average = 0,
                	total = 0,
                	numbers = 0,
                    count = 0,
                    dates = helper.getDateArray(daysToGoBack);
                async.whilst(
                    function() {
                        return count < dates.length;
                    },
                    function(wCb) {
                    	async.parallel({
                    		processReview: function(pCb) {
                    			redis.getFullArray(REVIEWS_PREFIX + 'list_' + id + "_" + dates[count], function(e, data) {
		                            reviewsDatewiseData.push({
		                                date: dates[count],
		                                reviews: data
		                            });
		                            count++;
		                            pCb(null);
		                        });
                    		},
                    		processAvg: function(pCb) {
                    			redis.get(REVIEWS_PREFIX + '_avg_' + id +"_" + dates[count], function(e, data) {
                    				console.log("AVG KEYS:", REVIEWS_PREFIX + '_avg_' + id +"_" + dates[count], e, data);
                    				if(!data)
                    					data = "0:0";
                    				data = data.split(":");
                    				total = parseInt(data[0], 10) + total;
                    				numbers = parseInt(data[1], 10) + numbers;
                    				pCb(null);
                    			})
                    		}
                    	}, function() {
                    		wCb(null);
                    	});
                        
                    },
                    function(e) {
                    	!numbers && (numbers = 1);
                        cb(e, reviewsDatewiseData, (total/numbers).toFixed(2));
                    }
                );
            }]
        }, function(e, reviewData, average) {
        	if(e) 
        		parentCb(null, {fetchData:["No Reviews"]}, 0);
        	else
            	parentCb(e, reviewData, average);
        });
    },
    processRecords: function(id, lsid, results, pageNo, q, parentCb, firstValue, monitFlag, qCallback) {
        var monitArr= [],
        	todayMinusDays = new Date();
        todayMinusDays.setHours(0);
        todayMinusDays.setMinutes(0);
        todayMinusDays.setSeconds(0);
        todayMinusDays.setMilliseconds(0);
        todayMinusDays.setDate(todayMinusDays.getDate() - daysToGoBack);
        // console.log("pageNo2:", pageNo, todayMinusDays);
        /*  ::::::::sample single object::::::::
			{ id: 'gp:AOqpTOG095fQEnSfKvNKRwxCjtMJxya0uq34CPorN4g3Up8ky_yBtIiefWiyX7S2fZEYXmXMp4EHlppYRApBDQ',
			  userName: '',
			  userImage: 'https://lh5.googleusercontent.com/-6mVPQnUKsGk/AAAAAAAAAAI/AAAAAAAAAAA/qHQ2NUX0m5o/w96-h96-p/photo.jpg',
			  date: 'October 31, 2017',
			  url: 'https://play.google.com/store/apps/details?id=com.goldvip.crownit&reviewId=Z3A6QU9xcFRPRzA5NWZRRW5TZkt2TktSd3hDanRNSnh5YTB1cTM0Q1Bvck40ZzNVcDhreV95QnRJaWVmV2l5WDdTMmZaRVlYbVhNcDRFSGxwcFlSQXBCRFE',
			  score: 5,
			  title: '',
			  text: 'OK',
			  replyDate: undefined,
			  replyText: undefined }
    	*/
        async.eachSeries(results.gpReview, function(single, eachCb) {
            // console.log("single:", single.date, single.id);

            // dont go forward you have all data after this id
            if (lsid && lsid.getLastSavedId && lsid.getLastSavedId == single.id) {
                // console.log(lsid.getLastSavedId, single.id, firstValue)
                redis.set(LAST_ID + id, firstValue);
                // stop here, you have all data after this record
                eachCb('stopGoingFurther');
            } else {
                if (new Date(single.date) >= todayMinusDays) {
                    // save to redis
                    var mySet,
                        date = new Date(single.date);
                    date = date.toString().split(" ");
                    date = '_' + date[2] + date[1] + date[3];

                    myAvg = REVIEWS_PREFIX + '_avg_' + id + date;
                    mySet = REVIEWS_PREFIX + 'set_' + id + date;
                    myList = REVIEWS_PREFIX + 'list_' + id + date;
                    redis.sIsMember(mySet, single.id, function(error, found) {
                        if (found == 0) {
                            // console.log("LIST:", myList);
                            if(monitFlag && single.score == 3) {
                            	monitArr.push(single.text);
                            }
                            redis.setExpire(myList, helper.getTTL(single.date, daysToGoBack));
                            redis.setExpire(mySet, helper.getTTL(single.date, daysToGoBack));
                            async.parallel({
                                list: function(cb) {
                                    redis.putArray(myList, [single.text], cb);
                                },
                                set: function(cb) {
                                    redis.addElementToSet(mySet, single.id, cb);
                                },
                                updateAvg: function(cb) {
                                	redis.get(myAvg, function(e, avg) {
                                		if(!avg) 
                                			avg = "0:0";
                                		avg = avg.split(":");
                                		avg = (parseInt(avg[0],10) + single.score) + ":" + (parseInt(avg[1],10) + 1);
                                		redis.set(myAvg, avg, helper.getTTL(single.date, daysToGoBack), cb);
                                	});
                                }
                            }, function() {
                                eachCb(null);
                            });
                        } else {
                            eachCb(null);
                        }
                    });
                } else {
                    // break
                    eachCb('lastDayReached');
                }
            }
        }, function(e) {
            if (e) {
                parentCb(null, monitArr);
            } else {
                q.push(pageNo[0]++);
                qCallback(null);
            }
        });
    },
    flushRedisDb: function(parentCb) {
    	redis.flushDB(parentCb);
    },
    monitor: function (id, parentCb) {
        var q, pageNo = [0],
            firstValue;
        async.auto({
            getLastSavedId: function(cb) {
                redis.get(LAST_ID + id, cb);
            },
            save2redis: ['getLastSavedId', function(lsid, cb) {
                q = async.queue(function(task, qCallback) {
                    // console.log("pageNo1:", pageNo);
                    c.fetchReviews(id, pageNo[0], null, function(e, results) {
                    	if(results.gpReview && results.gpReview.length == 0) {
                    		return cb('NoRecord');
                    	}
                        if (e) {
                            return cb(e);
                        } else {
                            // save it
                            if (!lsid.getLastSavedId) {
                                redis.set(LAST_ID + id, results && results.gpReview && results.gpReview[0] && results.gpReview[0].id);
                            }
                            !firstValue && (firstValue = results.gpReview[0].id);
                            exportMe.processRecords(id, lsid, results, pageNo, q, cb, firstValue, true, qCallback);
                        }
                    });
                }, 1);
                q.push(++pageNo[0]);
            }]
        }, function(e, reviewData) {
        	if(e) 
        		parentCb(null, "[]");
        	else
            	parentCb(e, JSON.stringify(reviewData.save2redis));
        });
    }
};

var helper = {
    getTTL: function(date, daysToGoBack) {
        var ttl = new Date(date);
        ttl.setHours(24);
        ttl.setMinutes(60);
        ttl.setSeconds(60);
        ttl.setMilliseconds(999);
        ttl.setDate(ttl.getDate() + daysToGoBack);
        ttl = ttl - new Date();
        ttl = parseInt(ttl / 100, 10);
        return ttl;
    },
    getDateArray: function(daysToGoBack) {
        var result = [];
        for (var i = 0; i < daysToGoBack; i++) {
            var d = new Date();
            d.setDate(d.getDate() - i);
            d = d.toString().split(" ");
            d = d[2] + d[1] + d[3];
            result.push(d);
        }
        return (result);
    }
};

module.exports = exportMe;
// exportMe.compute('com.dasharath.cipl', function (e, a) {
exportMe.monitor('c', function (e, a) {
    console.log("QWERTY:", e, a)
});