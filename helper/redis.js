var rtg   = require("url").parse(process.env.REDISTOGO_URL);

var redis = require('redis');
var async = require('async');

/* The class to connect with the Redis. It is used in the singleton method.
   It is initialized on the server class then used everywhere in the application. */
var Redis = (function () {
    var client;
    
    /* Initialize the client */
    function initializeClient() {
        client = redis.createClient(rtg.port, rtg.hostname);
        client.auth(rtg.auth.split(":")[1]);
        client.on('connect', function() {
            console.log('Redis client connected');
        });
    }
 
    /* Put a string in redis */
    function putString(key, value) {
        client.set(key, value);
    }
    /* Put an array in redis */
    function putArray(key, array, cb) {
        var multi = client.multi();
        async.each(array,
            function(value, callback) {
                multi.rpush(key, value); 
                callback(null);    
            },
            function(err) {
                if(err) {
                    console.log(err);
                }
                else {
                    multi.exec(function(errors, results) {
                        if(errors) {
                            console.log(errors);
                        }
                        else {
                            console.log(results);
                        }
                        cb && cb(errors, results);
                    });
                }
            }
        );
    }
    
    function get(key, callback) {
        client.get(key, callback);
    }

    function set(key, value, ttl, callback) {
        if(!callback) {
            callback = function() {};
        }
        if(ttl > 0) {
            client.set(key, value, 'EX', ttl, callback);
        } else {
            client.set(key, value);
        }
    }

    function getArray(key, start, end, callback) {
        client.lrange(key, start, end, callback);
    }

    function getArrayLength(key, callback) {
        client.llen(key, callback);
    }
    function getFullArray(key, callback) {
        this.getArrayLength(key, function(e, len) {
            client.lrange(key, 0, len, callback);
        });
    }
    function addElementToSet(setName, element, callback){
        if(!callback) {
            callback = function() {};
        }
        client.sadd(setName, element, callback);
        // callback(null, null);
    }
    function sIsMember(setName, element, callback){
        if(!callback) {
            callback = function() {};
        }
        client.sismember(setName, element, callback);
        // callback(null, null);
    }
    function setExpire(key, expiryTime) {
        client.expire(key, expiryTime);
    }
    function flushDB(callback) {
        client.flushdb(callback);
    }
    
    return {        
        get: get,
        set: set,
        flushDB: flushDB,
        getArray: getArray,
        putArray: putArray,
        putString: putString,
        setExpire: setExpire,
        sIsMember: sIsMember,
        getArrayLength: getArrayLength,
        getFullArray: getFullArray,
        addElementToSet: addElementToSet,
        init: function () {
            if (!client) {
                initializeClient();
            }
        }
    };
})();

module.exports = Redis;