var gplay = require('google-play-scraper');
var async = require('async');

var exportMe = {
    fetchReviews: function (id, pageNo, sortBy, callback) {
        (!sortBy) && (sortBy = gplay.sort.NEWEST);
        async.auto({
            gpReview: function (cb) {
                gplay.reviews({
                    appId: id,
                    page: pageNo,
                    sort: sortBy
                }).then(function (reviews) {
                    // console.log('Retrieved ' + reviews.length + ' reviews!', reviews);
                    cb(null, reviews);
                }).catch(function (e) {
                    console.log('thehere was an error fetching the reviews!');
                    cb(e);
                });
            }
        }, function (e, data) {
            callback(e, data);
        });
    }
}
// fetchReviews('com.monotype.whatthefont', 0, null, function(e, data) {
//     console.log("RR:",e,data)
// });

module.exports = exportMe;